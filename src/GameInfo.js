import React from "react";

const GameInfo = ({ status, playerCards, computerCards }) => {
  let result = "";
  if (status === 0) {
    result = "Valitse vertailtava ominaisuus?";
  } else if (status === 1) {
    result = `${playerCards > computerCards
      ? "Onneksi olkoon, voitit pelin!"
      : "Hävisit pelin!"}`;
  } else if (status === 2) {
    result = "Tasapeli, valitse uusi vertailtava ominaisuus?";
  } else if (status === 3) {
    result = "Voitit kortin, valitse uusi kortti?";
  } else if (status === 4) {
    result = "Hävisit kortin, valitse uusi kortti?";
  }
  return (
    <h3>
      {result}
    </h3>
  );
};

export default GameInfo;
