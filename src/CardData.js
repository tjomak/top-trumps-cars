import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Table, Image } from "react-bootstrap";

// make row of data cells with clickable rows
function colsTdButton(key, value, onClick, cardProperties, status) {
  let isDisabled = false;
  if (cardProperties.includes(key) && status === 2) {
    isDisabled = true;
  } else if (status === 1 || status > 2) {
    isDisabled = true;
  }
  if (isDisabled) {
    return (
      <tr key={key} className="tr-disabled">
        <td>
          {key}
        </td>
        <td>
          {value}
        </td>
      </tr>
    );
  } else {
    return (
      <tr key={key} className="tr-enabled" onClick={() => onClick(key)}>
        <td>
          {key}
        </td>
        <td>
          {value}
        </td>
      </tr>
    );
  }
}

// make row of data cells
function colsTd(key, value, cardProperties, status) {
  // highlight the last element in array
  if (cardProperties[cardProperties.length - 1] === key) {
    return (
      <tr key={key} className="tr-selected">
        <td>
          {key}
        </td>
        <td>
          {value}
        </td>
      </tr>
    );
  } else {
    return (
      <tr key={key}>
        <td>
          {key}
        </td>
        <td>
          {value}
        </td>
      </tr>
    );
  }
}
function CardData({ cardData, player, onClick, cardProperties, status }) {
  const keys = Object.keys(cardData);
  const values = Object.values(cardData);
  // this is the number of rows to show (image value not shown)
  const numberOfRows = values.length - 1;
  let rowsBody = [];

  // make first two rows card title
  const cardTitle = values[0] + " " + values[1];
  // put rest of the values to table
  for (let i = 2; i < numberOfRows; i++) {
    if (player) {
      rowsBody.push(
        colsTdButton(keys[i], values[i], onClick, cardProperties, status)
      );
    } else if (!player) {
      rowsBody.push(colsTd(keys[i], values[i], cardProperties, status));
    }
  }

  return (
    <div>
      <div className="card-image">
        <Image
          src={"/images/js/" + cardData.image}
          alt={cardTitle}
          responsive
        />
      </div>
      <div className="card-title">
        {cardTitle}
      </div>
      <Table striped>
        <tbody>
          {rowsBody}
        </tbody>
      </Table>
    </div>
  );
}
export default CardData;
