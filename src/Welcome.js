import React from "react";

const Welcome = ({ firstName, lastName }) =>
  <h3>
    Hello {firstName} {lastName}
  </h3>;

export default Welcome;
