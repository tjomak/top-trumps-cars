import React from "react";

const Results = ({ playerCards, computerCards }) =>
  <h3>
    Pelaaja: {playerCards} Tietokone: {computerCards}
  </h3>;

export default Results;
