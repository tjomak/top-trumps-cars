import React, { Component } from "react";
//import "./App.css";
import "./top-trumps.css";
import { shuffle } from "lodash";
import "bootstrap/dist/css/bootstrap.css";
import {
  ButtonGroup,
  Button,
  Col,
  Grid,
  Row,
  Image,
  OverlayTrigger,
  Popover
} from "react-bootstrap";
import GameInfo from "./GameInfo";
import CardData from "./CardData";
// import Welcome from "./Welcome";
// import Results from "./Results";

class App extends Component {
  constructor(props) {
    super(props);
    this.amountOfCards = 0;
    this.getCards();
    //0=start, 1=game over, 2=equal, 3=you won the card, 4=you lost the card
    this.status = 0;
    this.selectedProperties = [];
    this.buttonDisabled = true;
    this.state = {
      playerCards: [],
      computerCards: [],
      playerCard: {},
      computerCard: {},
      showComputerCard: false
    };
  }

  help = (
    <Popover id="popover-positioned-right" title="Pelin ohjeet">
      Tervetuloa pelaamaan Top Trumps peliä. Pelin tarkoituksena on valita
      kortista ominaisuus, minkä arvelisit olevan parempi kuin tietokoneen
      kortissa. Mikäli valitsemasi ominaisuus on parempi kuin tietokoneella,
      voitat kortin. Jos taas valitsemasi ominaisuus on huonompi kuin
      tietokoneella, häviät kortin. Tasapelin sattuessa sinun pitää valita jokin
      toinen ominaisuus. Se kumpi onnistuu saamaan kaikki kortit voittaa pelin.
    </Popover>
  );

  // fetch json data, which includes the cards data
  async getCards() {
    const response = await fetch("/top-packages-js.json");
    const data = await response.json();
    this.shareCards(data);
  }

  shareCards(data) {
    // count the amount of cards
    this.amountOfCards = data.length;
    // shuffle cards
    const cards = shuffle(data);
    let playerCards = [];
    let computerCards = [];
    for (let c = 0; c < this.amountOfCards; c++) {
      if (c % 2) {
        computerCards.push(cards[c]);
      } else {
        playerCards.push(cards[c]);
      }
    }
    this.setState({
      playerCards,
      computerCards
    });

    this.showCard();
  }

  showCard() {
    // show player card from top of the deck
    const playerCard = this.state.playerCards[0];
    // select also computers card ready to use
    const computerCard = this.state.computerCards[0];
    this.setState({
      playerCard,
      computerCard
    });
    // empty selectedProperties array
    this.selectedProperties.length = 0;
    this.buttonDisabled = true;
    this.status = 0;
  }

  getNewCard() {
    this.setState({ showComputerCard: false });
    this.showCard();
    // console.log("Uusi kortti:", this.counter);
    // console.log("status: ", this.status);
  }

  compareCards = value => {
    // console.log("pelaajan kortti: ", this.state.playerCard);
    // console.log("tietokoneen kortti: ", this.state.computerCard);
    const greaterWins = [
      "dependents",
      "downloadsLastMonth",
      "maintenance",
      "popularity",
      "quality",
      "releases"
    ];
    if (
      greaterWins.includes(value) &&
      this.state.playerCard[value] > this.state.computerCard[value]
    ) {
      // console.log("voitit kortin");
      this.status = 3;
    } else if (
      greaterWins.includes(value) &&
      this.state.playerCard[value] < this.state.computerCard[value]
    ) {
      // console.log("hävisit kortin");
      this.status = 4;
    } else if (
      !greaterWins.includes(value) &&
      this.state.playerCard[value] < this.state.computerCard[value]
    ) {
      // console.log("voitit kortin");
      this.status = 3;
    } else if (
      !greaterWins.includes(value) &&
      this.state.playerCard[value] > this.state.computerCard[value]
    ) {
      // console.log("hävisit kortin");
      this.status = 4;
    } else if (
      this.state.playerCard[value] === this.state.computerCard[value]
    ) {
      // console.log("tasapeli");
      this.status = 2;
    }

    if (this.status !== 2) {
      this.setState({ showComputerCard: true });
      this.buttonDisabled = false;
    } else {
      this.setState({ showComputerCard: false });
    }
    //this.selectedProperty = value;
    this.selectedProperties.push(value);
    // console.log("valitut ominaisuudet:", this.selectedProperties);
    this.updateCards();
  };

  updateCards() {
    let playerCards = this.state.playerCards;
    let computerCards = this.state.computerCards;
    let addCard = {};
    // if player wins, add computers card to player and add players card bottom of the deck
    if (this.status === 3) {
      playerCards.push(computerCards[0]);
      addCard = playerCards[0];
      playerCards.splice(0, 1);
      playerCards.push(addCard);
      computerCards.splice(0, 1);
    } else if (this.status === 4) {
      // if computer wins, add player card to computer and add computers card bottom of the deck
      computerCards.push(playerCards[0]);
      addCard = computerCards[0];
      computerCards.splice(0, 1);
      computerCards.push(addCard);
      playerCards.splice(0, 1);
    }
    this.setState({
      playerCards,
      computerCards
    });
    if (
      this.state.playerCards.length === this.amountOfCards ||
      this.state.computerCards.length === this.amountOfCards
    ) {
      this.status = 1;
      this.buttonDisabled = true;
    }
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col xs={12} sm={12} md={10} lg={8}>
            <div className="App-header">
              <h2>JavaScript Top Trumps</h2>
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={10} lg={8}>
            <div className="buttons">
              <ButtonGroup justified>
                <Button
                  href="#"
                  className="new-card"
                  bsStyle="primary"
                  disabled={this.buttonDisabled}
                  onClick={() => this.getNewCard()}
                >
                  Uusi kortti
                </Button>
                <Button
                  href="#"
                  className="new-game"
                  bsStyle="default"
                  disabled={this.status === 1 ? false : true}
                  onClick={() => window.location.reload()}
                >
                  Uusi peli
                </Button>
                <OverlayTrigger
                  trigger="click"
                  placement="bottom"
                  overlay={this.help}
                >
                  <Button href="#" bsStyle="info">
                    Ohjeet
                  </Button>
                </OverlayTrigger>
              </ButtonGroup>
            </div>
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs={12} sm={12} md={10} lg={8}>
            <GameInfo
              status={this.status}
              playerCards={this.state.playerCards.length}
              computerCards={this.state.computerCards.length}
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs={12} sm={6} md={5} lg={4}>
            <div className="player-title">
              Pelaajan kortti ({this.state.playerCards.length})
            </div>
            <div className="player-card">
              <CardData
                cardData={this.state.playerCard}
                player={true}
                onClick={this.compareCards}
                cardProperties={this.selectedProperties}
                status={this.status}
              />
            </div>
          </Col>
          <Col xs={12} sm={6} md={5} lg={4}>
            <div className="computer-title">
              Tietokoneen kortti ({this.state.computerCards.length})
            </div>
            <div className="computer-card">
              {this.state.showComputerCard
                ? <CardData
                    cardData={this.state.computerCard}
                    player={false}
                    cardProperties={this.selectedProperties}
                    status={this.status}
                  />
                : <Image src="/images/js/technologies.png" responsive />}
            </div>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default App;
